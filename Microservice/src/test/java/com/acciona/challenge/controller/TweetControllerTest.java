package com.acciona.challenge.controller;

import com.acciona.challenge.domain.entity.Tweet;
import com.acciona.challenge.service.TweetService;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@AutoConfigureMockMvc
@SpringBootTest
public class TweetControllerTest {
    @Autowired
    private TweetController controller;

    @MockBean
    private TweetService service;

    private static final String URL = "http://localhost:8080/api/tweets";

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        controller = new TweetControllerImpl(service);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void getTweet() throws Exception{
        Tweet tweet = new Tweet("61093b79be8d924d4476b9a9","Feli","Some Text","Spain", false);
        Gson gson = new Gson();
        String tweetJSON = gson.toJson(tweet);
        Mockito.when(service.getTweet(tweet.getId())).thenReturn(tweet);
        RequestBuilder request = MockMvcRequestBuilders.get(URL+ "/"+ tweet.getId());
        MvcResult result = mockMvc.perform(request).andReturn();
        assertEquals(tweetJSON,result.getResponse().getContentAsString());
    }

    @Test
    void postTweet() throws Exception{
        Tweet tweet = new Tweet("61093b79be8d924d4476b9a9","Feli","Some Text","Spain", false);
        Gson gson = new Gson();
        String tweetJSON = gson.toJson(tweet);
        Mockito.when(service.postTweet(tweet)).thenReturn(tweet);
        RequestBuilder request = MockMvcRequestBuilders.post(URL).contentType(MediaType.APPLICATION_JSON).content(String.valueOf(tweetJSON));
        MvcResult result = mockMvc.perform(request).andReturn();
        assertEquals(tweetJSON,result.getResponse().getContentAsString());
    }

    @Test
    void validateTweet() throws Exception{
        Tweet tweet = new Tweet("61093b79be8d924d4476b9a9","Feli","Some Text","Spain", false);
        Mockito.when(service.validateTweet(tweet.getId())).thenReturn(true);
        RequestBuilder request = MockMvcRequestBuilders.put(URL + "/" + tweet.getId() + "/validate");
        MvcResult result = mockMvc.perform(request).andReturn();
        assertEquals("true", result.getResponse().getContentAsString());
    }

    @Test
    void getAllTweets() throws Exception{
        Tweet tweet1 = new Tweet("61093b79be8d924d4476b9a9","Feli","Some Text","Spain", false);
        Tweet tweet2 = new Tweet("61093b79be8d924d44789099","Feli","Some Text","Spain", false);
        List<Tweet> mockedList = Arrays.asList(tweet1,tweet2);
        Gson gson = new Gson();
        String listJSON = gson.toJson(mockedList);
        Mockito.when(service.getAll()).thenReturn(mockedList);
        RequestBuilder request = MockMvcRequestBuilders.get(URL);
        MvcResult result = mockMvc.perform(request).andReturn();
        assertEquals(listJSON,result.getResponse().getContentAsString());
    }

    @Test
    void getValidatedTweetsByUser() throws Exception{
        Tweet tweet1 = new Tweet("61093b79be8d924d4476b9a9","Feli","Some Text","Spain", true);
        Tweet tweet2 = new Tweet("61093b79be8d924d44789099","Feli","Some Text","Spain", true);
        List<Tweet> mockedList = Arrays.asList(tweet1,tweet2);
        Gson gson = new Gson();
        String listJSON = gson.toJson(mockedList);
        Mockito.when(service.getValidatedTweetsByUser(tweet1.getUser())).thenReturn(mockedList);
        RequestBuilder request = MockMvcRequestBuilders.get(URL + "/"+ tweet1.getUser() + "/allValidated");
        MvcResult result = mockMvc.perform(request).andReturn();
        assertEquals(listJSON,result.getResponse().getContentAsString());
    }
}
