package com.acciona.challenge.controller;

import com.acciona.challenge.domain.entity.Hashtag;
import com.acciona.challenge.service.HashtagService;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@AutoConfigureMockMvc
@SpringBootTest
public class HashtagControllerTest {
    @Autowired
    private HashtagController controller;

    @MockBean
    private HashtagService service;

    private static final String URL = "http://localhost:8080/api/hashtags";

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        controller = new HashtagControllerImpl(service);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void topHashtags() throws Exception{
        Hashtag hashtag1 = new Hashtag("61093b79be8d924d449a9","Tokyo2020",23);
        Hashtag hashtag2 = new Hashtag("61093b7be8d924d4476a9","TestingHashtags",11);
        Hashtag hashtag3 = new Hashtag("61093b79be8d924d4476b9","HashtagHashtag",10);
        Hashtag hashtag4 = new Hashtag("1093b79be8d924d4476b9a9","HashtagHashtag",8);
        Hashtag hashtag5 = new Hashtag("6193b79be8d924d4476b9a9","HashtagHashtag",21);
        Hashtag hashtag6 = new Hashtag("6103b79be8d924d4476b9a9","HashtagHashtag",6);
        Hashtag hashtag7 = new Hashtag("61093b9be8d924d4476b9a9","HashtagHashtag",5);
        Hashtag hashtag8 = new Hashtag("61093b79e8d924d4476b9a9","HashtagHashtag",4);
        Hashtag hashtag9 = new Hashtag("61093b79be8d94d4476b9a9","HashtagHashtag",3);
        Hashtag hashtag10 = new Hashtag("61093b79be8d924d4476b99","HashtagHashtag",2);
        List<Hashtag> expected = Arrays.asList(hashtag1,hashtag5, hashtag2,hashtag3,hashtag4,hashtag6,hashtag7,hashtag8,hashtag9,hashtag10);
        Gson gson = new Gson();
        String hashtagJason = gson.toJson(expected);
        Mockito.when(service.topHashtags()).thenReturn(expected);
        RequestBuilder request = MockMvcRequestBuilders.get(URL+ "/topHashtags");
        MvcResult result = mockMvc.perform(request).andReturn();
        assertEquals(hashtagJason,result.getResponse().getContentAsString());
    }

    @Test
    void getAll() throws Exception{
        Hashtag hashtag1 = new Hashtag("61093b79be8d924d449a9","Tokyo2020",23);
        Hashtag hashtag2 = new Hashtag("61093b7be8d924d4476a9","TestingHashtags",11);
        Hashtag hashtag3 = new Hashtag("61093b79be8d924d4476b9","HashtagHashtag",10);
        Hashtag hashtag4 = new Hashtag("1093b79be8d924d4476b9a9","HashtagHashtag",8);
        List<Hashtag> expected = Arrays.asList(hashtag1, hashtag2,hashtag3,hashtag4);
        Gson gson = new Gson();
        String hashtagJason = gson.toJson(expected);
        //Aqui estara el fallo, supongo
        Mockito.when(service.getAll()).thenReturn(expected);
        RequestBuilder request = MockMvcRequestBuilders.get(URL);
        MvcResult result = mockMvc.perform(request).andReturn();
        assertEquals(hashtagJason,result.getResponse().getContentAsString());
    }
}
