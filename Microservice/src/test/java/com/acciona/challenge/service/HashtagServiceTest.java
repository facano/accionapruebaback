package com.acciona.challenge.service;

import com.acciona.challenge.domain.dto.HashtagDTO;
import com.acciona.challenge.domain.entity.Hashtag;
import com.acciona.challenge.repository.HashtagRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import twitter4j.HashtagEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class HashtagServiceTest {

    private HashtagService service;

    private HashtagRepository repo;

    @Autowired
    public HashtagServiceTest(HashtagRepository repo, HashtagService service) {
        this.service = service;
        this.repo = repo;
    }


    @Test
    public void topHashtags() {
        repo.deleteAll().block();
        Hashtag hashtag1 = new Hashtag(null,"Tokyo2020",23);
        Hashtag hashtag2 = new Hashtag(null,"TestingHashtags",11);
        Hashtag hashtag3 = new Hashtag(null,"HashtagHashtag",10);
        Hashtag hashtag4 = new Hashtag(null,"HashtagHashtag",8);
        Hashtag hashtag5 = new Hashtag(null,"HashtagHashtag",21);
        Hashtag hashtag6 = new Hashtag(null,"HashtagHashtag",6);
        Hashtag hashtag7 = new Hashtag(null,"HashtagHashtag",5);
        Hashtag hashtag8 = new Hashtag(null,"HashtagHashtag",4);
        Hashtag hashtag9 = new Hashtag(null,"HashtagHashtag",3);
        Hashtag hashtag10 = new Hashtag(null,"HashtagHashtag",2);
        Hashtag hashtag11 = new Hashtag(null,"HashtagHashtag",1);
        repo.save(hashtag1).block();repo.save(hashtag2).block();repo.save(hashtag3).block();repo.save(hashtag4).block();repo.save(hashtag5).block();
        repo.save(hashtag6).block();repo.save(hashtag7).block();repo.save(hashtag8).block();repo.save(hashtag9).block();repo.save(hashtag10).block();
        repo.save(hashtag11).block();
        List<Hashtag> expected = Arrays.asList(hashtag1,hashtag5, hashtag2,hashtag3,hashtag4,hashtag6,hashtag7,hashtag8,hashtag9,hashtag10);
        assertEquals(expected, service.topHashtags());
    }


    @Test
    public void topHashtagsWithShortage() {
        repo.deleteAll().block();
        Hashtag hashtag1 = new Hashtag(null,"Tokyo2020",23);
        Hashtag hashtag2 = new Hashtag(null,"TestingHashtags",5);
        Hashtag hashtag3 = new Hashtag(null,"HashtagHashtag",10);
        repo.save(hashtag1).block();repo.save(hashtag2).block();repo.save(hashtag3).block();
        List<Hashtag> expected = Arrays.asList(hashtag1, hashtag3,hashtag2);
        assertEquals(expected, service.topHashtags());
    }


    @Test
    public void getAll() {
        repo.deleteAll().block();
        Hashtag hashtag1 = new Hashtag(null,"Tokyo2020",23);
        Hashtag hashtag2 = new Hashtag(null,"TestingHashtags",5);
        Hashtag hashtag3 = new Hashtag(null,"HashtagHashtag",10);
        repo.save(hashtag1).block();repo.save(hashtag2).block();repo.save(hashtag3).block();
        List<Hashtag> expected = Arrays.asList(hashtag1, hashtag2,hashtag3);
        assertEquals(expected, service.getAll());

    }
}
