package com.acciona.challenge.service;

import com.acciona.challenge.domain.entity.Tweet;
import com.acciona.challenge.repository.TweetRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class TweetServiceTest {

    private TweetService service;

    private TweetRepository repo;

    @Autowired
    public TweetServiceTest(
            TweetRepository repo, TweetService service) {
        this.service = service;
        this.repo = repo;

    }

    @Test
    public void postTweet() {
        Tweet expected = new Tweet("610933debe8d94d4476b9a2", "Feli", "Some text", "Spain", false);
        Tweet actual = service.postTweet(expected);
        assertEquals(expected, actual);
    }

    @Test
    public void postNullTweet() {
        Tweet actual = service.postTweet(null);
        assertEquals(null, actual);
    }

    @Test
    public void getTweet() {
        Tweet expected = new Tweet("610933debe8d924d4476b9a2", "Feli", "Some text", "Spain", false);

        repo.save(expected).block();
        Tweet actual = service.getTweet("610933debe8d924d4476b9a2");
        System.out.println(expected);
        System.out.println(actual);
        assertEquals(expected, actual);
    }

    @Test
    public void getTweetInexistentId() {
        assertEquals(null, service.getTweet("610933debe8d924d4476b9a2"));
    }

    @Test
    public void validateTweet() {
        Tweet tweet = new Tweet("610933debe8d924d4476b9a2", "Feli", "Some text", "Spain", false);
        repo.save(tweet).block();
        assertEquals(true, service.validateTweet("610933debe8d924d4476b9a2"));
    }

    @Test
    public void validateTweetInexistentId() {
        assertEquals(false, service.validateTweet("InexistentId"));
    }

    @Test
    public void getValidatedTweetsByUser() {
        Tweet expected1 = new Tweet("610933debe8d924d4476b9a2", "Feli", "Some text", "Spain", true);
        Tweet expected2 = new Tweet("610933debe8d924d4476b9a3", "Feli", "Some text", "Spain", true);
        Tweet expected3 = new Tweet("610933debe8d924d4476b9a4", "Feli", "Some text", "Spain", false);
        repo.save(expected1).block();
        repo.save(expected2).block();
        repo.save(expected3).block();
        List<Tweet> expectedList = Arrays.asList(expected1, expected2);

        assertEquals(expectedList, service.getValidatedTweetsByUser("Feli"));
    }

    @Test
    public void getValidatedTweetsByInexistentUser() {
        assertEquals("[]", service.getValidatedTweetsByUser("InexistentUser").toString());
    }


    @Test
    public void getAll() {
        Tweet expected1 = new Tweet("610933debe8d924d4476b9a2", "Feli", "Some text", "Spain", true);
        Tweet expected2 = new Tweet("610933debe8d924d4476b9a3", "Hiberus", "Some text", "Spain", true);
        Tweet expected3 = new Tweet("610933debe8d924d4476b9a4", "Acciona", "Some text", "Spain", false);
        repo.save(expected1).block();
        repo.save(expected2).block();
        repo.save(expected3).block();
        List<Tweet> expectedList = Arrays.asList(expected1, expected2, expected3);
        assertEquals(expectedList, service.getAll());
    }

}
