package com.acciona.challenge.utils;

import com.acciona.challenge.domain.entity.Hashtag;
import com.acciona.challenge.domain.entity.Tweet;
import com.acciona.challenge.repository.HashtagRepository;
import com.acciona.challenge.repository.TweetRepository;
import com.acciona.challenge.service.HashtagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

@Component
public class TwitterStreamApi implements ApplicationListener<ApplicationReadyEvent> {


    @Value("${oauth.consumerKey}")
    private String consumerKey;
    @Value("${oauth.consumerSecret}")
    private String consumerSecret;
    @Value("${oauth.accessToken}")
    private String accessToken;
    @Value("${oauth.accessTokenSecret}")
    private String accessTokenSecret;
    @Value("${identitest}")
    private boolean esTest;

    private final TweetRepository repo;
    private final HashtagRepository repoHashtag;
    private final HashtagService service;

    private final ValidarTweets valid;


    @Autowired
    public TwitterStreamApi(TweetRepository repo, HashtagRepository repoHashtag, HashtagService service, ValidarTweets valid) {
        this.repo = repo;
        this.repoHashtag = repoHashtag;
        this.service = service;
        this.valid = valid;
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        if (!esTest) {
            this.startStreamApi();
        }
    }

    public void startStreamApi() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(consumerKey)
                .setOAuthConsumerSecret(consumerSecret)
                .setOAuthAccessToken(accessToken)
                .setOAuthAccessTokenSecret(accessTokenSecret);
        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();

        Flux<Status> tweets = Flux.create(sink -> {
            twitterStream.onStatus(sink::next);
            twitterStream.onException(sink::error);
            sink.onCancel(twitterStream::shutdown);
            twitterStream.sample();
        });


        tweets
                .filter(valid::validate)
                .flatMap(t -> repo.save(Tweet.fromStatus(t)))
                .subscribe();


        tweets
                .filter(valid::validate)
                .map(t ->service.countHashtags(Hashtag.fromStatus(t)))
                .subscribe();

    }
}
