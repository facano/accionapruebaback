package com.acciona.challenge.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import twitter4j.Status;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
public class ValidarTweets {


    @Value("${followerLowerLimit}")
    private int followerLowerLimit;
    @Value("${langList}")
    private List<String> langList;

    public Boolean validate(Status status) {
        return status.getUser().getFollowersCount() > followerLowerLimit
                && langList.contains(status.getLang());
    }
}