package com.acciona.challenge.service;

import com.acciona.challenge.domain.entity.Hashtag;
import com.acciona.challenge.repository.HashtagRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import java.util.Arrays;
import java.util.List;

@Log4j2
@Service
public class HashtagServiceImpl implements HashtagService{

    @Value("${topHashtagsLength}")
    private int topHashtagsLength;

    private final HashtagRepository repo;

    @Autowired
    public HashtagServiceImpl(HashtagRepository repo) {
        this.repo = repo;
    }

    @Override
    public List<Hashtag> topHashtags() {
        List<Hashtag> list = repo.findAll().collectList().block();
        list.sort(Hashtag::compareTo);


        int aux;
        if(list.size()<=topHashtagsLength){
            aux=list.size();
        }else{
            aux = topHashtagsLength;
        }
        Hashtag[] out = new Hashtag[aux];
        for(int i = 0; i< aux; i++)
            out[i]=list.get(i);

        return Arrays.asList(out);
    }

    @Override
    public Flux<Hashtag> countHashtags(Flux<Hashtag> hashtags) {

        Flux<Hashtag> tmp = hashtags.flatMap(t->
                repo.findByContent(t.getContent())
        .defaultIfEmpty(new Hashtag(null, t.getContent(), 0))
        .map(hd->{
            hd.increment();
            return hd;
        })
        );
        repo.saveAll(tmp).subscribe();
        return tmp;
    }

    @Override
    public List<Hashtag> getAll() {
        return repo.findAll().collectList().block();
    }
}
