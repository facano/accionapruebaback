package com.acciona.challenge.service;

import twitter4j.Status;

public interface TwitterStreamService {
    boolean saveStream(Status status);
}
