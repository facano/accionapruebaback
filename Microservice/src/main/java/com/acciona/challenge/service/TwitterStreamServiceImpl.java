package com.acciona.challenge.service;

import com.acciona.challenge.domain.entity.Tweet;
import com.acciona.challenge.repository.TweetRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import twitter4j.Status;
import java.util.Arrays;
import java.util.List;

@Log4j2
@Service
public class TwitterStreamServiceImpl implements TwitterStreamService {

    @Value("${followerLowerLimit}")
    private int followerLowerLimit;
    @Value("${langList}")
    private List<String> langList;

    @Autowired
    private HashtagService service;

    private final TweetRepository repo;

    public TwitterStreamServiceImpl(TweetRepository repo) {
        this.repo = repo;
    }

    public Boolean validate(Status status) {
        return status.getUser().getFollowersCount() > followerLowerLimit
                && langList.contains(status.getLang());
    }

    @Override
    public boolean saveStream(Status status) {
        if(langList.contains(status.getLang()) && status.getUser().getFollowersCount()>=followerLowerLimit){
            Tweet tweet = new Tweet(null,status.getUser().getName(), status.getText(), status.getUser().getLocation(), false);
            repo.save(tweet).subscribe().dispose();
            //service.countHashtags(Arrays.asList(status.getHashtagEntities()));
            return true;
        }
        return false;
    }

}
