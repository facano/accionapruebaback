package com.acciona.challenge.service;

import com.acciona.challenge.domain.entity.Tweet;
import java.util.List;

public interface TweetService {
    Tweet postTweet(Tweet tweet);
    Tweet getTweet(String id);
    boolean validateTweet(String id);
    List<Tweet> getValidatedTweetsByUser(String user);
    List<Tweet> getAll();
}
