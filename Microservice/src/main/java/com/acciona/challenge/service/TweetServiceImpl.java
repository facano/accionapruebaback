package com.acciona.challenge.service;

import com.acciona.challenge.domain.entity.Tweet;
import com.acciona.challenge.repository.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class TweetServiceImpl implements TweetService{

    private final TweetRepository repo;

    @Autowired
    public TweetServiceImpl(TweetRepository repo) {
        this.repo = repo;
    }

    @Override
    public Tweet postTweet(Tweet tweet) {

        if(tweet==null)return null;
        if(repo.existsById(tweet.getId()).block())return null;
        return repo.save(tweet).block();

    }

    @Override
    public  Tweet getTweet(String id) {
        if(!repo.existsById(id).block())return null;
            return repo.findById(id).block();

    }

    @Override
    public boolean validateTweet(String id) {
        if(!repo.existsById(id).block())return false;
            Tweet antiguo = repo.findById(id).block();
            antiguo.setValidation(true);
            repo.save(antiguo);
            return true;

    }

    @Override
    public List<Tweet> getValidatedTweetsByUser(String user) {
        //No es necesario manejo de usuario inexitente, mongo ya lo proporciona
        return repo.findByUserAndValidation(user,true).collectList().block();
    }

    @Override
    public List<Tweet> getAll() {
        return repo.findAll().collectList().block();
    }
}
