package com.acciona.challenge.service;

import com.acciona.challenge.domain.entity.Hashtag;
import reactor.core.publisher.Flux;
import twitter4j.HashtagEntity;
import java.util.List;

public interface HashtagService {
    List<Hashtag> topHashtags();
    Flux<Hashtag> countHashtags(Flux<Hashtag> hashtags);
    List<Hashtag> getAll();
}
