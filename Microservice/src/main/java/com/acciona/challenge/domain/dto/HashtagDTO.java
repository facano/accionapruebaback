package com.acciona.challenge.domain.dto;

import twitter4j.HashtagEntity;

public class HashtagDTO implements HashtagEntity {

    public String text;
    public int start;
    public int end;

    public  HashtagDTO(String text, int start, int end){
        this.text=text;
        this.start=start;
        this.end=end;
    }

    @Override
    public String getText() {
        return this.text;
    }

    @Override
    public int getStart() {
        return 0;
    }

    @Override
    public int getEnd() {
        return 0;
    }


}
