package com.acciona.challenge.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import twitter4j.HashtagEntity;
import twitter4j.Status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("Hashtags")

public class Hashtag implements Comparable<Hashtag>{

    @Id
    public String id;
    public String content;
    public int count;

    public int compareTo(Hashtag o) {
        if(count<o.getCount()){
            return 1;
        }
        if(count>o.getCount()){
            return -1;
        }
        return 0;
    }

    public void increment(){
        this.count=count +1;
    }

    public static Flux<Hashtag> fromStatus(Status s) {

        ArrayList<Hashtag> x = new ArrayList<>();

        Arrays.stream(s.getHashtagEntities()).forEach(
                hashtagEntity -> {
                    x.add(new Hashtag(null, hashtagEntity.getText(), 1));
                }
        );
        Hashtag[] xx = new Hashtag[x.size()];

        for (int i = 0; i < x.size(); i++) {
            xx[i] = x.get(i);
        }

        return Flux.just(xx);
    }
}
