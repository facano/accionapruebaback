package com.acciona.challenge.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import twitter4j.HashtagEntity;
import twitter4j.Status;

import java.util.Arrays;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("Tweets")
@Builder
public class Tweet {
    @Id
    public String id;
    public String user;
    public String text;
    public String location;
    public boolean validation;
    
    public static Tweet fromStatus(Status s){
        return new Tweet(null,s.getUser().getName(), s.getText(), s.getUser().getLocation(), false);
    }

}
