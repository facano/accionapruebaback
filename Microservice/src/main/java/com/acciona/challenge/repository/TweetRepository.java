package com.acciona.challenge.repository;

import com.acciona.challenge.domain.entity.Tweet;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;


public interface TweetRepository extends ReactiveMongoRepository<Tweet, String> {
    Flux<Tweet> findByUserAndValidation(String user, boolean validation);
}
