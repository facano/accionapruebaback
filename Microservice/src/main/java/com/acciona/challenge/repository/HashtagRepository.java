package com.acciona.challenge.repository;

import com.acciona.challenge.domain.entity.Hashtag;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface HashtagRepository extends ReactiveMongoRepository<Hashtag, String> {
    Mono<Hashtag> findByContent(String content);
}
