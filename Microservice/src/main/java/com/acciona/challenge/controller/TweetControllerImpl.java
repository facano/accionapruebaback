package com.acciona.challenge.controller;

import com.acciona.challenge.domain.entity.Tweet;
import com.acciona.challenge.service.TweetService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(path = "/api/tweets")
public class TweetControllerImpl implements TweetController{

    private final TweetService tweetService;

    @Autowired
    public TweetControllerImpl(TweetService tweetService) {
        this.tweetService = tweetService;
    }

    @Override
    @PostMapping("")
    @ApiOperation(value = "Post tweet", notes = "Post a tweet and saves it in the database")
    public ResponseEntity<Tweet> postTweet(@RequestBody Tweet tweet) {
        return ResponseEntity.ok(tweetService.postTweet(tweet));
    }

    @Override
    @GetMapping("")
    @ApiOperation(value = "Get all tweets", notes = "Gets all tweets stored in the database")
    public ResponseEntity<List<Tweet>> getAll() {
        return ResponseEntity.ok(tweetService.getAll());
    }

    @Override
    @GetMapping("/{id}")
    @ApiOperation(value = "Get tweet", notes = "Gets a tweet from the database given its id")
    public ResponseEntity<Tweet> getTweet(@PathVariable String id) {
        return ResponseEntity.ok(tweetService.getTweet(id));
    }

    @Override
    @PutMapping("/{id}/validate")
    @ApiOperation(value = "Validate tweet", notes = "Validates a tweet from the database given its id")
    public ResponseEntity<Boolean> validateTweet(@PathVariable String id) {
        return ResponseEntity.ok(tweetService.validateTweet(id));
    }

    @Override
    @GetMapping("/{user}/allValidated")
    @ApiOperation(value = "Gets validated tweets by user", notes = "Gets a list of validated tweets from a given user")
    public ResponseEntity<List<Tweet>> getValidatedTweetsByUser(@PathVariable String user) {
        return ResponseEntity.ok(tweetService.getValidatedTweetsByUser(user));
    }
}
