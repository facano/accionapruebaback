package com.acciona.challenge.controller;

import com.acciona.challenge.domain.entity.Tweet;
import org.springframework.http.ResponseEntity;
import java.util.List;

public interface TweetController {
    ResponseEntity<Tweet> postTweet(Tweet tweet);
    ResponseEntity<Tweet> getTweet(String id);
    ResponseEntity<Boolean> validateTweet(String id);
    ResponseEntity<List<Tweet>> getValidatedTweetsByUser(String user);
    ResponseEntity<List<Tweet>> getAll();
}
