package com.acciona.challenge.controller;

import com.acciona.challenge.domain.entity.Hashtag;
import com.acciona.challenge.service.HashtagService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping(path = "/api/hashtags")
public class HashtagControllerImpl implements HashtagController{

    private final HashtagService service;

    @Autowired
    public HashtagControllerImpl(HashtagService service) {
        this.service = service;
    }

    @Override
    @GetMapping("/topHashtags")
    @ApiOperation(value = "Gets top Hashtags", notes = "Gets as many number of the most used hashtags as determined in the run configuration (Default 10)")
    public ResponseEntity<List<Hashtag>> topHashtags() {
        return ResponseEntity.ok(service.topHashtags());
    }

    @Override
    @GetMapping("")
    @ApiOperation(value = "Get all tweets", notes = "Gets all tweets stored in the database")
    public ResponseEntity<List<Hashtag>> getAll() {
        return ResponseEntity.ok(service.getAll());
    }
}
