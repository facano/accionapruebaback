package com.acciona.challenge.controller;

import com.acciona.challenge.domain.entity.Hashtag;
import org.springframework.http.ResponseEntity;
import java.util.List;

public interface HashtagController {
    ResponseEntity<List<Hashtag>> topHashtags();
    ResponseEntity<List<Hashtag>> getAll();
}
