# Prueba Acciona

Instrucciones de ejecución del proyecto.

1. Descargar el proyecto
2. En el application.properties de la ruta `Microservice > src > main > resources > application.properties` es necesario poner las "Consumer keys" en `oauth.consumerKey` y `oauth.consumerSecret`. También es necesario poner los tokens de autentificación en `oauth.accessToken` y `oauth.accessTokenSecret`. Las claves de commits anteriores no funcionan, dado que tienen un limite de solicitud de tweets y ya se ha alcanzado.
3. Ejecutar desde el directorio raiz el comando: `docker-compose up --build`

Una vez levantado el contenedor el servicio estará disponible en el puerto 8080



```
Se han documentado los endpoints mediante Swagger. Una vez activo el contenedor se puede acceder mediante:
http://localhost:8080/swagger-ui/index.htm

```

# Consideraciones:
1. Debido a las limitaciones de obtención de datos de la API de twitter, es posible que en cierto punto el stream se cierre por exceso de peticiones. Normalmente sucede al haber solicitado grandes cantidades de tweets. 
2. Es posible ejecutar el servicio mediante el archivo "MicroserviceApplication" para no depender de docker, en caso de desearlo
